﻿/**********************************************************************************************
***********************************************************************************************
Ceci est une bibliothèque pour la simplification de l'utilisation de fichiers csv.
***********************************************************************************************
***********************************************************************************************
Ce code est soumis à la licence GNU GPL V3
***********************************************************************************************
***********************************************************************************************
Copyright 2015 Alexandre Croteau
***********************************************************************************************
**********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace acsvparse
{
    public class csvfile
    {
        private Dictionary<string, int> dheaders;
        private string[] linetemplate;
        private List<string[]> lines;

        private string vfilename;

        private void csvfromfile(string filename)//privé car assignation seulement... utiliser le constructeur
        {
            vfilename = filename;
            lines = new List<string[]>();
            read();
        }

        public void setheadersfromfirstline()//public pour permettre d'ouvrir un fichier sans savoir si headers et ajouter plus tard
        {
            int nbcol = lines.FirstOrDefault().Length;
            dheaders = new Dictionary<string, int>(nbcol);
            for (int i = 0; i < nbcol; i++)
            {
                dheaders.Add(lines[0][i], i);
            }
        }

        public csvfile(int colcount, bool hasheaders)//nouveau fichier à partir de rien
        {
            linetemplate = new string[colcount];

            if(hasheaders)
            {
                dheaders = new Dictionary<string, int>(colcount);
            }

            lines = new List<string[]>();
        }

        public csvfile(string filename)//à partir d'un fichier existant sans headers
        {
            csvfromfile(filename);
        }

        public csvfile(string filename, bool hasheaders)//à partir d'un fichier existant avec ou sans headers
        {
            csvfromfile(filename);

            if (hasheaders)
            {
                setheadersfromfirstline();//ajoute les headers
            }
        }

        public bool setheaders(string[] pheaders)
        {
            if(pheaders.Length!=dheaders.Count)
            {
                return false;
            }
            else
            {
                int i = 0;
                foreach(string item in pheaders)
                {
                    dheaders.Add(item, i);
                    i++;
                }
                //s'assurer que le header n'est pas dans la liste de lignes si possible
                return true;
            }
        }

        public void setheaders(string[] pheaders, int nbcol)
        {
            int i = 0;
            foreach (string item in pheaders)
            {
                dheaders.Add(item, i);
                i++;
            }
            linetemplate = new string[nbcol];
            //s'assurer que le header n'est pas dans la liste de lignes si possible
        }

        public string[] getheaders()
        {
            string[] retour = new string[dheaders.Count];
            foreach(var item in dheaders)
            {
                retour[item.Value] = item.Key;
            }
            return retour;
        }

        public List<string[]> getlines()
        {
            //etre sur de ne pas retourner le header si possible
            return lines;
        }

        public bool addline(string[] line)
        {
            if (line.Length == linetemplate.Length)
            {
                lines.Add(line);
                return true;
            }
            else
            {
                return false;
            }
        }

        public string filename
        {
            get
            {
                return vfilename;
            }
            set
            {
                vfilename = value;
            }
        }

        public string getvalue(string columnname, int? columnno, int rowno)//obtenir une colonne a une ligne precise
        {
            int col=0;
            if(columnno!=null)
                col= (int)columnno;
            
            if(columnname!="")//on cherche le no de colonne
            {
                col = dheaders.Single(x => x.Key == columnname).Value;
            }

            return lines[rowno][col];
        }

        public string[] getcolumn(string columnname, int? columnno)//obtenir les valeurs pour une colonne
        {
            int col = 0;
            List<string> values = new List<string>();

            if (columnno != null)
                col = (int)columnno;

            if (columnname != "")//on cherche le no de colonne
            {
                col = dheaders.Single(x => x.Key == columnname).Value;
            }

            foreach(string[] item in lines)
            {
                values.Add(item[col]);
            }

            return values.ToArray();
        }

        public bool read()
        {
            try
            {
                string[] file = File.ReadAllLines(vfilename);
                foreach(string li in file)
                {
                    string[] pli = li.Split(';');
                    addline(pli);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool savetodisk()
        {
            try
            {
                List<string> flines = new List<string>();
                foreach(string[] line in lines)
                {
                    flines.Add(string.Join(";", line));
                }
                File.WriteAllLines(vfilename, flines);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public void removeat(int rowno)
        {
            lines.RemoveAt(rowno);
        }

        public void insertat(int rowno, string[]line)
        {
            lines.Insert(rowno, line);
        }
    }
}
